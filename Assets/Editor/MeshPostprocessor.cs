﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class MeshPostprocessor : AssetPostprocessor {

	void OnPreprocessModel(){
		(assetImporter as ModelImporter).globalScale = 1;
		}
}
