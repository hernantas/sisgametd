﻿using UnityEngine;
using System.Collections;

public class UtilityAsset : MonoBehaviour {
	public Texture2D healthFgTex;
	public Texture2D healthBgTex;
	public GUIStyle guiStyle;
	public Texture2D bgPanel;
	public Texture2D btnTex;
	public Texture2D btnTowerTex;
	public Texture2D btnTowerBgTex;
	public Texture2D statusBar;
	public Texture2D statusBarBg;
	public Texture2D liveIcon;
	public Texture2D goldIcon;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static bool CheckGUIClick()
	{
		GUILayer l = Camera.main.GetComponent(typeof(GUILayer)) as GUILayer;
		GUIElement ele = l.HitTest(Input.mousePosition);
		if(ele != null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
