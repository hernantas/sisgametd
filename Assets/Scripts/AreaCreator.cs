﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AreaCreator : MonoBehaviour {
	public enum GroundType
	{
		GT_GRASS,
		GT_START,
		GT_FINISH,
		GT_CONCRETE,
		GT_WATER,
		GT_TALL_GRASS
	};
	public GameObject[] quadType;
	public GroundType[,] areaMap;

	// Use this for initialization
	void Start () {
		/*
			000000000000000000
			000000000000000000
			00000..00..0000000
			00000..00..0000000
			00000..00..0000000
			00.....00.......00
			00.....00.......00
			00..0000000000..00
			00..0000000000..00
			00..00......00..00
			00..00......00..00
			00......00..00..00
			00......00..00..00
			0000000000..00..00
			0000000000..00..00
			00..........00..00
			00..........00..00
			00..0000000000..00
			00..0000000000..00
			00..............00
			00..............00
			000000000000000000
			000000000000000000
		*/
		string map = "" +
				"0000000000000000000\n" +
				"0000000000000000000\n" +
				"000000S000F00000000\n" +
				"000000.000.00000000\n" +
				"000000.000.00000000\n" +
				"000....000......000\n" +
				"000.00000000000.000\n" +
				"000.00000000000.000\n" +
				"000.00000000000.000\n" +
				"000.000.....000.000\n" +
				"000.000.000.000.000\n" +
				"000.....000.000.000\n" +
				"00000000000.000.000\n" +
				"00000000000.000.000\n" +
				"00000000000.000.000\n" +
				"000.........000.000\n" +
				"000.00000000000.000\n" +
				"000.00000000000.000\n" +
				"000.00000000000.000\n" +
				"000.............000\n" +
				"0000000000000000000\n" +
				"0000000000000000000\n" +
				"0000000000000000000\n";

		GameObject parentField = new GameObject ("FieldContainer");
		parentField.isStatic = true;

		int rowLength = 0;
		int j = 0;
		for (int i=0;i<map.Length;i++)
		{
			if (map[i] == '\n')
			{
				if (j==0)rowLength++;
				j++;
			}
			else
			{
				if (j==0)
					rowLength++;
			}
		}

		Debug.Log (j + "|" + rowLength);
		areaMap = new GroundType[j,rowLength];
		j = 0;
		rowLength = 0;

		for (int i=0;i<map.Length;i++)
		{
			if (map[i] == '\n')
			{
				if (j==0)rowLength++;
				j++;
			}
			else
			{
				if (j==0)
					rowLength++;
				
				if (map[i] == '0')
					areaMap[j,(i%rowLength)] = GroundType.GT_GRASS;
				if (map[i] == '.')
					areaMap[j,(i%rowLength)] = GroundType.GT_CONCRETE;
				if (map[i] == 'S')
					areaMap[j,(i%rowLength)] = GroundType.GT_START;
				if (map[i] == 'F')
					areaMap[j,(i%rowLength)] = GroundType.GT_FINISH;
			}
		}

		for (int y=0;y<j; y++)
		{
			for (int x=0;x<rowLength-1;x++)
			{
				GameObject go = null;

				if (areaMap[y,x] == GroundType.GT_GRASS || areaMap[y,x] == GroundType.GT_START || areaMap[y,x] == GroundType.GT_FINISH)
					go = (GameObject) Instantiate(quadType[0], new Vector3(x*-5,0,y*5), Quaternion.identity);

				if (areaMap[y,x] == GroundType.GT_CONCRETE)
					go = (GameObject) Instantiate(quadType[1], new Vector3(x*-5,0,y*5), Quaternion.identity);

				go.layer = LayerMask.NameToLayer("Field");
				go.transform.Rotate(new Vector3(90,0,0));
				go.isStatic = true;
				go.transform.parent = parentField.transform;
			}
		}

		parentField.transform.Translate (new Vector3 (27.5f, 0, -2.5f));
		/*
		areaMap = new GroundType[23,18];

		//y,x
		for (int y=0; y<23; y++)
			for (int x=0; x<18; x++)
				areaMap [y, i] = GroundType.GT_TALLGRASS;

		for (int y=3; y< 21; y++)
			for (int x=2; x<16; x++)
				areaMap [y, i] = GroundType.GT_GRASS;

		areaMap [3, 5] = GroundType.GT_CONCRETE;
		areaMap [3, 6] = GroundType.GT_CONCRETE;
		areaMap [4, 5] = GroundType.GT_CONCRETE;
		areaMap [4, 6] = GroundType.GT_CONCRETE;
		areaMap [5, 5] = GroundType.GT_CONCRETE;
		areaMap [5, 6] = GroundType.GT_CONCRETE;
		areaMap [6, 5] = GroundType.GT_CONCRETE;
		areaMap [6, 6] = GroundType.GT_CONCRETE;
		areaMap [4, 5] = GroundType.GT_CONCRETE;
		areaMap [4, 5] = GroundType.GT_CONCRETE;
		*/

		StartPathFinding ();
	}

	void StartPathFinding()
	{
		int x = 0; int y = 0;
		int fx = 0; int fy = 0;

		//Debug.Log (areaMap.GetLength (0) + "." + areaMap.GetLength (1));

		for (int _y = 0; _y < areaMap.GetLength(0); _y++)
		{
			for (int _x = 0; _x < areaMap.GetLength(1); _x++)
			{
				if (areaMap[_y,_x] == GroundType.GT_START)
				{
					x = _x;
					y = _y;
				}

				if (areaMap[_y,_x] == GroundType.GT_FINISH)
				{
					fx = _x;
					fy = _y;
				}
			}
		}

		List<Vector3> path = new List<Vector3> ();
		Vector2 lastDirection = new Vector2 (0, 0);
		int maxIter = 100;
		int lx = x, ly = y;
		path.Add(new Vector3(x*-5+27.5f, 0, y*5-2.5f));
		
		while ((x != fx || y != fy) && maxIter > 0)
		{
			Vector2 nextPath = FindPath(x,y, lx, ly);

			if (nextPath.x == -1 && nextPath.y == -1)
			{
				//Debug.Log("Invalid");
				break;
			}
			Vector2 nextDirection = nextPath - (new Vector2(x,y));

			if (nextDirection != lastDirection)
			{
				path.Add(new Vector3(nextPath.x*-5+27.5f, 0, nextPath.y*5-2.5f));
				lastDirection = nextDirection;
			}
			else
			{
				path[path.Count-1] = new Vector3(nextPath.x*-5+27.5f, 0, nextPath.y*5-2.5f);
			}

			lx = x;
			ly = y;
			x = (int)nextPath.x;
			y = (int)nextPath.y;
			maxIter --;
		}

		GetComponent<Spawner> ().WaypointList.Clear ();
		GetComponent<Spawner> ().WaypointList = path;
	}

	Vector2 FindPath(int x, int y, int lx, int ly)
	{
		//Debug.Log (x + "." + y);
		if (checkValidArea(x,y+1, lx, ly))
		{
			return new Vector2(x, y+1);
		}
		else if (checkValidArea(x+1,y, lx, ly))
		{
			return new Vector2(x+1, y);
		}
		else if (checkValidArea(x,y-1, lx, ly))
		{
			return new Vector2(x, y-1);
		}
		else if (checkValidArea(x-1,y, lx, ly))
		{
			return new Vector2(x-1, y);
		}
		else 
			return new Vector2(-1,-1);
	}

	bool checkValidArea(int x, int y, int lx, int ly)
	{
		//Debug.Log (x + ".." + y + "(" + lx + ", " + ly + ")");
		if (x < 0 || y < 0 || y >= areaMap.GetLength(0) || x >= areaMap.GetLength(1))
		{
			return false;
		}
		if (areaMap[y,x] == GroundType.GT_CONCRETE && !(lx == x && ly == y))
		{
			return true;
		}

		return false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
