﻿using UnityEngine;
using System.Collections;

public class AbilityTitan : Ability 
{
	
	public AbilityTitan()
	{
		name = "Titan";
		cooldown = 99999999999;
		duration = 1;
	}
	
	protected override void OnCast ()
	{
		caster.MaxHealth = caster.MaxHealth * 4;
		caster.Health = caster.MaxHealth;
		caster.MoveSpeed = caster.baseMoveSpeed / 2;
		Vector3 scale = caster.transform.localScale;
		scale = scale * 2;
		caster.transform.localScale = scale;
	}
}
