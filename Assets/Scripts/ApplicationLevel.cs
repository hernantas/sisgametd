﻿using UnityEngine;
using System.Collections;

public enum QuestStatus
{
	IDLE,
	ONPROGRESS,
	FAIL,
	SUCCESS_1,
	SUCCESS_2,
	SUCCESS_3
}

public class ApplicationLevel
{
	public static int UnlockedMonster = 2;
	public static bool Continous = true;
	public static int Lives = 10;
	public static int LevelToStart = 10;
	public static QuestStatus QuestStatus = QuestStatus.IDLE;
}
