﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour 
{
	private int waveAtStart = 1;
	public int Wave = 1;
	public int WavePerLevel = 5; 
	public int Level
	{
		set
		{
			Wave = WavePerLevel*(value-1)+1;
		}
		get
		{
			return Mathf.FloorToInt(Wave / (float)WavePerLevel);
		}
	}
	private int curTotalSpawn = 0;
	public int TotalSpawn = 10;
	public bool isContinous = true;
	private bool isFinished = false;
	public bool IsEnd
	{
		get
		{
			return (!isContinous && isFinished);
		}
	}
	public GameObject[] monsterList;
	public float RateSpawnTime = 0.5f;
	private float curSpawnTime = 0f;
	public List<Vector3> WaypointList;
	private float curDelay = 0f;
	public float Delay = 14f;
	private int curUnlockedMonster = 1;
	private int unlockedMonster = 1;
	private GUIStyle guiStyle = null;

	// Use this for initialization
	void Start ()
	{
		guiStyle = new GUIStyle(Camera.main.GetComponent<UtilityAsset> ().guiStyle);
		guiStyle.normal.background = GetComponent<UtilityAsset> ().statusBarBg;
		guiStyle.alignment = TextAnchor.MiddleCenter;

		resetStatus ();
		// Delay long time at start
		curDelay = 30.0f;
		unlockedMonster = ApplicationLevel.UnlockedMonster;
		isContinous = ApplicationLevel.Continous;
		Level = ApplicationLevel.LevelToStart;
		waveAtStart = Wave;
	}

	void OnGUI()
	{
		if (curDelay > 0 && GetComponent<Quest>().status == QuestStatus.ONPROGRESS)
			GUI.Label (new Rect (Screen.width/2, 20, 140, 32), "Next Spawn in: " + (int)curDelay + "s", guiStyle);
	}

	void resetStatus()
	{
		TotalSpawn = 10 + (Mathf.FloorToInt ((Wave - 1) / (float)WavePerLevel) * 5);
		Debug.Log (TotalSpawn);
		curDelay = Delay;
		curUnlockedMonster = 1;
		curTotalSpawn = 0;

		if (isContinous)
		{
			float percent = GetComponent<Player>().Live/GetComponent<Player>().MaxLive;
			GetComponent<Player>().MaxLive = 10 + Level;
			GetComponent<Player>().Live = Mathf.FloorToInt(GetComponent<Player>().MaxLive * percent);
		}
	}

	void Spawn()
	{
		if (curDelay <= 0)
		{
			if (curSpawnTime > RateSpawnTime && curTotalSpawn < TotalSpawn)
			{
				curSpawnTime = 0;
				GameObject go = (GameObject) Instantiate(monsterList[Random.Range(0,unlockedMonster)], 
				                                         this.WaypointList[0],
				                                         Quaternion.identity);
				go.tag = "Monster";
				Monster mcs = go.GetComponent<Monster>();
				// Set monster ability
				Ability[] abs = go.GetComponents<Ability>();
				
				for (int i=0;i<abs.Length;i++)
				{
					int random = Random.Range(0,100);
					
					if (random > abs[i].Chance || Level < abs[i].MinimumLevel)
					{
						Destroy(go.GetComponent(abs[i].GetType()));
					}
				}
				
				// Set Health Monster
				int inc = Mathf.CeilToInt(Wave/(float)WavePerLevel);
				int dec = Mathf.CeilToInt(Mathf.Floor(Wave-((Wave-1)%WavePerLevel))*inc*0.95f);
				mcs.Health = mcs.baseHealth + Wave*inc-dec;
				mcs.MaxHealth = mcs.Health;
				mcs.WaypointList = WaypointList;
				curTotalSpawn++;
				curUnlockedMonster++;

				if (curTotalSpawn >= TotalSpawn)
				{
					go.AddComponent<AbilityTitan>();
				}
			}
			
			curSpawnTime += Time.deltaTime;
			
			if (curTotalSpawn == TotalSpawn)
			{
				if (isContinous || (!isContinous && Wave < waveAtStart+WavePerLevel-1))
				{
					Wave++;
					resetStatus();
				}
				else if (GameObject.FindGameObjectsWithTag("Monster").Length == 0)
				{
					isFinished = true;
					Player player = GetComponent<Player>();
					if (player.Live/(float)player.MaxLive >= 1)
						GetComponent<Quest>().status = QuestStatus.SUCCESS_3;
					else if (player.Live/(float)player.MaxLive >= 0.5f)
						GetComponent<Quest>().status = QuestStatus.SUCCESS_2;
					else if (player.Live >= 1)
						GetComponent<Quest>().status = QuestStatus.SUCCESS_1;
					else
						GetComponent<Quest>().status = QuestStatus.FAIL;	
				}
			}
		}
		else
		{
			curDelay -= Time.deltaTime;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (isFinished == false && GetComponent<Quest>().status == QuestStatus.ONPROGRESS)
		{
			Spawn();
		}
		else
		{
		}
	}
}
