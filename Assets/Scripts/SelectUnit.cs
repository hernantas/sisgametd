﻿using UnityEngine;
using System.Collections;

public class SelectUnit : MonoBehaviour {
	public GameObject selectionObj = null;
	public GameObject AOEObj = null;
	private GameObject curSelected = null;
	private bool showUpgrade = false;
	private bool ignoreSelect = false;
	private GUIStyle guiStyle;
	private GUIStyle btnStyle;

	// Use this for initialization
	void Start () {
		selectionObj.renderer.enabled = false;
		AOEObj.renderer.enabled = false;
		guiStyle = new GUIStyle(GetComponent<UtilityAsset> ().guiStyle);

		btnStyle = new GUIStyle(GetComponent<UtilityAsset> ().guiStyle);
		btnStyle.normal.background = GetComponent<UtilityAsset> ().btnTex;
		btnStyle.alignment = TextAnchor.MiddleCenter;
	}

	void OnGUI()
	{
		if (curSelected != null)
		{
			Tower tw = curSelected.GetComponent<Tower>();
			if (!tw.InBuildMode)
			{
				showUpgrade = tw.level < tw.maxLevel;
				GUI.DrawTexture(new Rect(Screen.width-180, (Screen.height*1/3)-40, 140, 130), GetComponent<UtilityAsset>().bgPanel);
				GUI.Label(new Rect(Screen.width-150, (Screen.height*1/3)-32, 120, 32), curSelected.gameObject.name + " Lv. " + tw.level, guiStyle);
				GUI.Label(new Rect(Screen.width-170, (Screen.height*1/3), 60, 32), "Damage: ", guiStyle);
				GUI.Label(new Rect(Screen.width-90, (Screen.height*1/3), 60, 32), ((int)tw.Damage).ToString() + (showUpgrade?" (+"+((int)(tw.DamageNextLevel-tw.Damage))+")":""), guiStyle);
				GUI.Label(new Rect(Screen.width-170, (Screen.height*1/3)+32, 60, 32), "Aspd:", guiStyle);
				GUI.Label(new Rect(Screen.width-90, (Screen.height*1/3)+32, 60, 32), (tw.AttackSpeed).ToString("n2"), guiStyle);
				GUI.Label(new Rect(Screen.width-170, (Screen.height*1/3)+64, 60, 32), "Range:", guiStyle);
				GUI.Label(new Rect(Screen.width-90, (Screen.height*1/3)+64, 60, 32), ((int)tw.Range).ToString(), guiStyle);

				Rect btnRect = new Rect(Screen.width-180, (Screen.height*1/3)+104, 120, 32);
				if (tw.level < tw.maxLevel && GUI.Button(btnRect, "Upgrade "+tw.PriceNextLevel+" G", btnStyle))
				{
					ignoreSelect = true;

					if (GetComponent<Player>().Gold < tw.PriceNextLevel)
						GetComponent<Player>().MessageToPlayer("Not enough gold");
					else
					{
						GetComponent<Player>().Gold -= tw.PriceNextLevel;
						tw.LevelUp();
					}
				}
			}
			else
			{
				curSelected = null;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (curSelected == null)
			AOEObj.renderer.enabled = false;
		else
			AOEObj.transform.Rotate(new Vector3(0,0,10 * Time.deltaTime));

		if (Input.GetMouseButtonUp(0) && !ignoreSelect)
		{
			curSelected = getMousePointObject();
		}

		if (curSelected != null)
		{
			Vector3 pos = curSelected.transform.position;
			AOEObj.renderer.enabled = true;
			pos.y = 0.01f;
			AOEObj.transform.position = pos;
			float scale = curSelected.GetComponent<Tower>().Range*2;
			AOEObj.transform.localScale = new Vector3(scale, scale, 1);
		}

		GameObject mouseHover = getMousePointObject ();
		if (mouseHover != null && !mouseHover.GetComponent<Tower>().InBuildMode)
		{
			selectionObj.renderer.enabled = true;
			Vector3 pos = mouseHover.transform.position;
			pos.y = 0.01f;
			selectionObj.transform.position = pos;

		}
		else
			selectionObj.renderer.enabled = false;

		if (Input.GetMouseButtonUp(1))
			curSelected = null;
	}

	void LateUpdate()
	{
		ignoreSelect = false;
	}

	public void forceSelect(GameObject go)
	{
		curSelected = go;
	}

	public void ignoreSelection()
	{
		this.ignoreSelect = true;
	}

	GameObject getMousePointObject()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		
		if (Physics.Raycast(ray, out hit, 250, LayerMask.GetMask("Tower")))
		{
			return hit.transform.gameObject;
		}
		return null;
	}
}
