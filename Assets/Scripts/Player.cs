﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{
	private int live = 10;
	private int maxLive = 10;
	public int Live
	{
		set
		{
			live = value;

			if (live > maxLive)
				live = maxLive;
			else if (live == 0)
				ApplicationLevel.QuestStatus = QuestStatus.FAIL;
		}
		get
		{
			return live;
		}
	}
	public int MaxLive
	{
		set
		{
			maxLive = value;
		}
		get
		{
			return maxLive;
		}
	}
	public int Gold = 100;
	private float cameraSpeed = 40;
	private float timeDisplay = 5;
	private float zoomSpeed = 0;
	private GUIStyle errorStyle = new GUIStyle();
	private GUIStyle messageStyle = new GUIStyle();
	private Queue message = new Queue();
	private GUIStyle guiStyle;
	private GUIStyle liveStyle;
	private Texture2D liveBg;
	private Texture2D liveIcon;
	private Texture2D goldIcon;
	private GUIStyle goldStyle;

	// Use this for initialization
	void Start () 
	{
		MaxLive = ApplicationLevel.Lives;
		Live = ApplicationLevel.Lives;
		guiStyle = new GUIStyle(GetComponent<UtilityAsset> ().guiStyle);

		errorStyle = new GUIStyle(guiStyle);
		errorStyle.normal.textColor = Color.red;

		liveStyle = new GUIStyle (guiStyle);
		liveStyle.normal.background = GetComponent<UtilityAsset> ().statusBarBg;
		liveStyle.alignment = TextAnchor.MiddleCenter;
		liveStyle.normal.textColor = new Color (1,1,1);
		liveBg = GetComponent<UtilityAsset> ().statusBar;
		liveIcon = GetComponent<UtilityAsset> ().liveIcon;

		goldStyle = new GUIStyle (liveStyle);
		goldStyle.normal.textColor = Color.yellow;
		goldIcon = GetComponent<UtilityAsset> ().goldIcon;
	}

	void OnGUI()
	{
		if (GetComponent<Quest>().status == QuestStatus.ONPROGRESS)
		{
			GUI.DrawTexture (new Rect (Screen.width-((128*Live/maxLive)+10), 10	, 128*Live/maxLive, 32), liveBg);
			GUI.Label (new Rect (Screen.width-138, 10	, 128, 32), Live.ToString(), liveStyle);
			GUI.DrawTexture (new Rect (Screen.width - 47, 10, 32, 32), liveIcon);

			GUI.Label (new Rect (Screen.width-((128)+10), 52, 128, 32), Gold.ToString(), goldStyle);
			GUI.DrawTexture (new Rect (Screen.width - 47, 52, 32, 32), goldIcon);
			//GUI.Label (new Rect (120+5, 10, 120, 42), "Interest in: " + (int)curTime + "s (7s)", guiStyle);

			if (message.Count > 0)
			{
				foreach(string obj in message)
				{
					GUI.Label(new Rect(Screen.width/2-(obj.ToString().Length*5/2),Screen.height*1/4,Screen.width,50), obj.ToString(), errorStyle);
					break;
				}

				timeDisplay -= Time.deltaTime;

				if (timeDisplay < 0)
				{
					timeDisplay = 5;
					message.Dequeue();
				}
			}
			else
				timeDisplay = 5;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		ControlCamera ();
	}

	public void MessageToPlayer(string msg)
	{
		this.message.Enqueue (msg);
		timeDisplay = 5;

		if (this.message.Count > 1)
			this.message.Dequeue();
	}

	void ControlCamera()
	{
		Vector3 direction = Vector3.zero;
		int multiplier = 1;

		if (Input.GetKey (KeyCode.LeftShift)) 
		{
			multiplier = 2;
		}

		if (Input.GetKey (KeyCode.W))
			direction += new Vector3 (0, 0, 1);
		if (Input.GetKey (KeyCode.A))
			direction += new Vector3 (-1, 0, 0);
		if (Input.GetKey (KeyCode.S))
			direction += new Vector3 (0, 0, -1);
		if (Input.GetKey (KeyCode.D))
			direction += new Vector3 (1, 0, 0);

		float axis = Input.GetAxis ("Mouse ScrollWheel");

		if (axis != 0)
		{
			zoomSpeed += (axis*20);
		}
		else
		{
			zoomSpeed -= zoomSpeed/2;
		}

		direction.Normalize ();
		direction += Camera.main.transform.rotation * (new Vector3 (0, 0, 1)) * zoomSpeed;

		this.transform.parent.transform.Translate (direction * 
		                                           multiplier * cameraSpeed * 
		                                           Time.deltaTime);
	}
}
