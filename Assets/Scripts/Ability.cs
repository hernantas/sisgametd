﻿using UnityEngine;
using System.Collections;

public class Ability : MonoBehaviour
{
	protected Monster caster;
	protected new string name;
	private float _cooldown;
	protected float cooldown;
	private float _duration;
	public float currentDuration
	{
		get
		{
			return _duration;
		}
	}
	protected float duration;
	[SerializeField]
	protected int minLevel = 5;
	public int MinimumLevel
	{
		get 
		{
			return minLevel;
		}
	}
	[SerializeField]
	protected int chance = 10;
	public int Chance
	{
		get
		{
			return chance;
		}
	}

	public void cast()
	{
		if (_cooldown <= 0 && CastCondition())
		{
			_cooldown = cooldown;
			_duration = duration;
			this.OnCast();
		}
	}

	protected virtual bool CastCondition()
	{
		return true;
	}

	protected virtual void OnCast()
	{
	}

	protected virtual void OnDuration()
	{
	}

	protected virtual void OnFinish()
	{
	}

	void Awake()
	{
		caster = GetComponent<Monster> ();
		chance = Mathf.Clamp (chance, 0, 100);
	}

	void Update()
	{
		if (_duration <= 0 && _cooldown > 0)
			_cooldown -= Time.deltaTime;

		if (_duration > 0)
		{
			this.OnDuration();
			if (_duration - Time.deltaTime <= 0)
			{
				this.OnFinish();
			}
			_duration -= Time.deltaTime;
		}
	}
}
