﻿using UnityEngine;
using System.Collections;

public class Builder : MonoBehaviour
{
	public GameObject[] TowerList;
	public Texture2D[] IconTower;
	private GameObject curTowerBuild = null;
	private Material[] buildMatDefault = null;
	public Material buildMatAccept = null;
	public Material buildMatError = null;
	private bool mouseBtnDelay = false;
	private GUIStyle btnGuiSyle;
	private GUIStyle btnLblStyle;
	private Texture2D btnBgTex;

	void Start()
	{
		btnBgTex = GetComponent<UtilityAsset> ().btnTowerBgTex;
		btnGuiSyle = new GUIStyle (GetComponent<UtilityAsset> ().guiStyle);
		btnGuiSyle.normal.background = GetComponent<UtilityAsset> ().btnTowerTex;
		btnLblStyle = new GUIStyle (GetComponent<UtilityAsset> ().guiStyle);
		btnLblStyle.alignment = TextAnchor.LowerRight;
		btnLblStyle.normal.textColor = Color.white;
	}

	void OnGUI()
	{
		if (GetComponent<Quest>().status == QuestStatus.ONPROGRESS)
		{
			float btnWidth = 100;
			float btnHeight = 100;
			float btnPadding = 5;

			for(int i=0;i<TowerList.Length;i++)
			{
				GUI.DrawTexture(new Rect(Screen.width - ((btnWidth+btnPadding)*(TowerList.Length-i)) +5
				                         , Screen.height-(btnHeight+btnPadding) + 5
				                         , btnWidth-10 , btnHeight-10), btnBgTex);
				GUI.DrawTexture(new Rect(Screen.width - ((btnWidth+btnPadding)*(TowerList.Length-i)) +5
				                         , Screen.height-(btnHeight+btnPadding) + 5
				                         , btnWidth-10 , btnHeight-10), IconTower[i]);
				if (GUI.Button(new Rect(Screen.width - ((btnWidth+btnPadding)*(TowerList.Length-i))
				                        , Screen.height-(btnHeight+btnPadding)
				                        , btnWidth , btnHeight), "", btnGuiSyle))
				{
					if (curTowerBuild != null)
						DestroyObject(curTowerBuild);

					Vector3 pos = getMousePoint();
					mouseBtnDelay = true;
					pos.y = TowerList[i].renderer.bounds.size.y/2;
					curTowerBuild = (GameObject) Instantiate(TowerList[i], pos, Quaternion.identity);
					curTowerBuild.name = TowerList[i].name;
					buildMatDefault = curTowerBuild.renderer.materials;
					curTowerBuild.GetComponent<Tower>().InBuildMode = true;
				}
				GUI.Label(new Rect(Screen.width - ((btnWidth+btnPadding)*(TowerList.Length-i))
				                   , Screen.height-(btnHeight+btnPadding)
				                   , btnWidth-10 , btnHeight-5), TowerList[i].GetComponent<Tower>().Price.ToString() + " G", btnLblStyle);
			}
		}
	}
	
	void Update()
	{
		bool canBuild = false;
		Vector2 iPos = new Vector2(0,0);

		if (curTowerBuild != null) 
		{
			Vector3 pos = getMousePoint();
			
			pos.x = Mathf.Max((pos.x-Mathf.Abs(pos.x%5)),
			                  ((Mathf.Floor(pos.x/5)+1)*5))-2.5f;
			pos.z = Mathf.Max((pos.z-Mathf.Abs(pos.z%5)),
			                  ((Mathf.Floor(pos.z/5)+1)*5))-2.5f;
			pos.y = curTowerBuild.transform.position.y;
			curTowerBuild.transform.position = pos;
			
			iPos = getGridIndex(pos);
			Material[] mtrs = new Material[curTowerBuild.renderer.materials.Length];
			if (GetComponent<AreaCreator>().areaMap[(int)iPos.y,(int)iPos.x] == AreaCreator.GroundType.GT_GRASS)
			{
				canBuild = true;

				for (int i=0;i<curTowerBuild.renderer.materials.Length;i++)
				{
					mtrs[i] = buildMatAccept;
				}
			}
			else
			{
				for (int i=0;i<curTowerBuild.renderer.materials.Length;i++)
				{
					mtrs[i] = buildMatError;
				}
			}

			curTowerBuild.renderer.materials = mtrs;
		}

		if (Input.GetMouseButtonUp (1) && !this.mouseBtnDelay && curTowerBuild != null) 
		{
			Destroy (curTowerBuild);
			curTowerBuild = null;
		}
		if (Input.GetMouseButtonUp (0) && !this.mouseBtnDelay && curTowerBuild != null && !UtilityAsset.CheckGUIClick()) 
		{
			Player ply = GetComponent<Player>();
			Tower tw = curTowerBuild.GetComponent<Tower>();
			
			if (ply.Gold < tw.Price)
			{
				DestroyObject(curTowerBuild);
				GetComponent<Player>().MessageToPlayer("Not enough gold");
			}
			else if (!canBuild)
			{
				GetComponent<Player>().MessageToPlayer("Can't build here");
			}
			else
			{
				GetComponent<SelectUnit>().ignoreSelection();
				GetComponent<AreaCreator>().areaMap[(int)iPos.y,(int)iPos.x] = AreaCreator.GroundType.GT_CONCRETE;
				ply.Gold -= tw.Price;
				GetComponent<SelectUnit>().forceSelect(curTowerBuild);
				curTowerBuild.GetComponent<Tower>().InBuildMode = false;
				curTowerBuild.renderer.materials = buildMatDefault;
				curTowerBuild = null;
			}
		}
		if (Input.GetMouseButtonUp (0) || Input.GetMouseButtonUp (1))
			this.mouseBtnDelay = false;
	}

	Vector2 getGridIndex(Vector3 pos)
	{
		int x = (int) (pos.x - (pos.x % 2.5f) - 27.5f);
		x = x / 5 * -1;
		int y = (int) (pos.z = pos.z - (pos.z%2.5f) + 5f);
		y = y / 5;
		return new Vector2 (x, y);
	}

	public static Vector3 getMousePoint()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		
		if (Physics.Raycast(ray, out hit, 250, LayerMask.GetMask("Field")))
		{
			return hit.point;
		}
		return Vector3.zero;
	}
}

/*
public class Builder : MonoBehaviour {
	public GameObject[] TowerList;
	public GameObject curBuild = null;
	private bool mouseBtnDelay = false;
	private Material curMaterial;
	public Material MaterialAcc;
	public Material MaterialErr;

	// Use this for initialization
	void Start () 
	{
	}

	void OnGUI()
	{
		float width = 100;
		float height = 100; 
		float padding = 5;

		for (int i=0; i<TowerList.Length; i++) 
		{
			if (GUI.Button (new Rect (Screen.width - ((width+padding)*(TowerList.Length-i))
			                      , Screen.height-(height+padding)
			                      , width , height), TowerList[i].name))
			{
				if (curBuild != null)
					Destroy(curBuild);

				Vector3 pos = getMousePoint();
				pos.y = TowerList[i].renderer.bounds.size.y/2;

				this.mouseBtnDelay = true;
				curBuild = (GameObject) Instantiate(TowerList[i], pos, Quaternion.identity);
				curBuild.name = TowerList[i].name;
				curMaterial = curBuild.renderer.material;
			}
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		bool canBuild = false;
		Vector2 ipos = new Vector2(0,0);
		if (curBuild != null) 
		{
			Vector3 pos = getMousePoint();

			pos.x = Mathf.Max((pos.x-Mathf.Abs(pos.x%5)),
			                  ((Mathf.Floor(pos.x/5)+1)*5))-2.5f;
			pos.z = Mathf.Max((pos.z-Mathf.Abs(pos.z%5)),
			                  ((Mathf.Floor(pos.z/5)+1)*5))-2.5f;
			pos.y = curBuild.transform.position.y;
			curBuild.transform.position = pos;

			ipos = getGridIndex(pos);
			if (GetComponent<AreaCreator>().areaMap[(int)ipos.y,(int)ipos.x] == AreaCreator.GroundType.GT_GRASS)
			{
				canBuild = true;
				curBuild.renderer.material = MaterialAcc;
			}
			else
			{
				curBuild.renderer.material = MaterialErr;
			}
		}

		if (Input.GetMouseButtonUp (1) && !this.mouseBtnDelay && curBuild != null) 
		{
			Destroy (curBuild);
			curBuild = null;
		}
		if (Input.GetMouseButtonUp (0) && !this.mouseBtnDelay && curBuild != null && !UtilityAsset.CheckGUIClick()) 
		{
			Player ply = GetComponent<Player>();
			Tower tw = curBuild.GetComponent<Tower>();
			
			if (ply.Gold < tw.Price)
			{
				DestroyObject(curBuild);
				GetComponent<Player>().MessageToPlayer("Not enough gold");
			}
			else if (!canBuild)
			{
				GetComponent<Player>().MessageToPlayer("Can't build here");
			}
			else
			{
				GetComponent<AreaCreator>().areaMap[(int)ipos.y,(int)ipos.x] = AreaCreator.GroundType.GT_CONCRETE;
				ply.Gold -= tw.Price;
				GetComponent<SelectUnit>().forceSelect(curBuild);
				curBuild.GetComponent<Tower>().setActive(true);
				curBuild.renderer.material = curMaterial;
				curBuild = null;
			}
		}
		if (Input.GetMouseButtonUp (0) || Input.GetMouseButtonUp (1))
			this.mouseBtnDelay = false;
	}

	Vector2 getGridIndex(Vector3 pos)
	{
		int x = (int) (pos.x - (pos.x % 2.5f) - 27.5f);
		x = x / 5 * -1;
		int y = (int) (pos.z = pos.z - (pos.z%2.5f) + 5f);
		y = y / 5;
		return new Vector2 (x, y);
	}
	Vector3 getMousePoint()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		
		if (Physics.Raycast(ray, out hit, 250, LayerMask.GetMask("Field")))
		{
			return hit.point;
		}
		return Vector3.zero;
	}
}
*/
