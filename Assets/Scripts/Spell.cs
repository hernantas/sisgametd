﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpellCasted
{
	public float duration;
	public Vector3 castPos;
	public float tick;
	public GameObject isntancedEffect;
	public bool resetTick = false;
}

public class Spell : MonoBehaviour {
	public string Name;
	// public GameObject Effect;
	public Texture2D icon;
	public float areaOfEffect;
	public float duration;
	public int cost = 10;
	public string affectedTag;
	[SerializeField]
	private int minLevel = 2;
	public int MinimumLevel
	{
		get
		{
			return minLevel;
		}
	}
	private List<SpellCasted> spellCasteds;
	public List<GameObject> affected;
	public GameObject spellEffect;

	public void cast (Vector3 pos)
	{
		pos.y = 0.1f;
		SpellCasted sc = new SpellCasted ();
		sc.castPos = pos;
		sc.duration = duration;
		sc.isntancedEffect = (GameObject)Instantiate (spellEffect);
		sc.isntancedEffect.transform.localScale = new Vector3 (areaOfEffect, areaOfEffect, areaOfEffect);
		sc.isntancedEffect.transform.position = sc.castPos;
		spellCasteds.Add (sc);
	}

	public virtual void OnObjectAdded(GameObject go)
	{
	}
	public virtual void OnObjectRemoved(GameObject go)
	{
	}
	public virtual void OnTick(GameObject go, SpellCasted sc)
	{
	}

	void Start()
	{
		spellCasteds = new List<SpellCasted> ();
		affected = new List<GameObject>();
	}

	void Update()
	{
		GameObject[] gos = GameObject.FindGameObjectsWithTag (affectedTag);

		List<GameObject> newAffected = new List<GameObject>();
		for(int i=0;i<spellCasteds.Count;i++)
		{
			SpellCasted sc = spellCasteds[i];
			sc.duration -= Time.deltaTime;
			sc.tick += Time.deltaTime;
			sc.isntancedEffect.transform.Rotate(0,0,100*Time.deltaTime);

			foreach(GameObject go in gos)
			{
				Vector3 pos = go.transform.position;
				pos.y = sc.castPos.y;
				if (Vector3.Distance(pos, sc.castPos) < areaOfEffect/2+2 && 
				    ((go.tag.Equals("Tower") && !go.GetComponent<Tower>().InBuildMode) || (go.tag.Equals("Monster") && !go.GetComponent<Monster>().IsDeath)))
				{
					newAffected.Add(go);
				}
			}

			for (int j=0;j<affected.Count;j++)
			{
				this.OnTick(affected[j], spellCasteds[i]);
			}

			/*
			for(int j=0;j<affected.Count;j++)
			{
				GameObject go = affected[j];

				if (go == null || (affectedTag == "Monster" && go.GetComponent<Monster>().IsDeath))
				{
					affected.Remove(go);
					j--;
					continue;
				}

				this.OnTick(go, spellCasteds[i]);

				if (sc.duration <= 0)
				{
					this.OnObjectRemoved(go);
				}
				else if (Vector3.Distance(sc.castPos, go.transform.position) > areaOfEffect)
				{
					this.OnObjectRemoved(go);
					affected.Remove(go);
					j--;
				}
			}

			if (sc.duration <= 0)
				affected.Clear();
			else
				foreach(GameObject go in gos)
				{
					bool alreadyInList = false;
					foreach(GameObject gf in affected)
					{
						if (gf == go)
						{
							alreadyInList = true;
							break;
						}
					}

					if (alreadyInList)
						continue;
				else if (Vector3.Distance(sc.castPos, go.transform.position)-(go.renderer.bounds.size.x+go.renderer.bounds.size.z)/2.0f <= areaOfEffect/2.0f && ((go.tag == "Tower" && !go.GetComponent<Tower>().InBuildMode) || go.tag == "Monster" ))
					{
						OnObjectAdded(go);
						affected.Add(go);
					}
				}
			*/

			if (sc.duration <= 0)
			{
				Destroy(sc.isntancedEffect);
				spellCasteds.Remove(sc);
				i--;
			}

			if (sc != null && sc.resetTick)
			{
				sc.tick = 0;
				sc.resetTick = false;
			}
		}

		// Add Object
		foreach(GameObject go in newAffected)
		{
			bool found = false;
			for (int j=0;j<affected.Count;j++)
			{
				if (go == affected[j])
				{
					found = true;
					break;
				}
			}
			
			if (!found)
			{
				this.OnObjectAdded(go);
				affected.Add(go);
			}
		}
		
		// Remove Object
		for (int j=0;j<affected.Count;j++)
		{
			bool found = false;
			foreach(GameObject go in newAffected)
			{
				if (go == affected[j])
				{
					found = true;
					break;
				}
			}
			
			if (!found)
			{
				this.OnObjectRemoved(affected[j]);
				affected.RemoveAt(j);
				j--;
			}
		}
	}
}
