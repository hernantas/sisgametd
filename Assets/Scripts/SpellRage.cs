﻿using UnityEngine;
using System.Collections;

public class SpellRage : Spell {
	private float defDuration;
	SpellRage()
	{
		Name = "Rage";
		areaOfEffect = 10;
		duration = 10;
		affectedTag = "Tower";
	}
	public override void OnTick(GameObject go, SpellCasted sc)
	{
		float dr = ((duration - sc.duration)/duration);
		Tower tw = go.GetComponent<Tower> ();
		BonusAttribute ba = new BonusAttribute ();
		ba.name = "Rage";
		ba.damage = tw.BaseDamage;
		ba.speed = 2;
		tw.transform.localScale = new Vector3 (1.5f, 1.5f, 1.5f);
		tw.attrMgr.Add (ba);
	}
	public override void OnObjectRemoved(GameObject go)
	{
		Tower tw = go.GetComponent<Tower> ();
		tw.attrMgr.Remove ("Rage");
		tw.transform.localScale = new Vector3 (1,1,1);
	}
}
