﻿using UnityEngine;
using System.Collections;

public class AbilityHaste : Ability 
{

	public AbilityHaste()
	{
		name = "Haste";
		cooldown = 30;
		duration = 3;
	}

	protected override void OnCast ()
	{
		BonusAttribute ba = new BonusAttribute ();
		ba.name = "Haste";
		ba.priority = Mathf.CeilToInt(currentDuration * 100);
		ba.speed = caster.baseMoveSpeed * currentDuration/duration;
		caster.attrMgr.Add (ba);
	}

	protected override void OnDuration ()
	{
		BonusAttribute ba = new BonusAttribute ();
		ba.name = "Haste";
		ba.priority = Mathf.CeilToInt(currentDuration * 100);
		ba.speed = caster.baseMoveSpeed * currentDuration/duration;
		caster.attrMgr.Add (ba);
	}

	protected override void OnFinish()
	{
		caster.attrMgr.Remove ("Haste");
	}
}
