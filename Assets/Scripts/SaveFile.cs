﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public static class SaveFile 
{
	public static Dictionary<string, string> Property = new Dictionary<string, string>();

	public static void Load()
	{
		Property.Clear ();
		string path = Application.persistentDataPath + "/playerprogress.sav";

		if (!File.Exists (path))
			File.Create (path);

		StreamReader stream = new StreamReader (path);

		while (!stream.EndOfStream)
		{
			string line = stream.ReadLine();
			string[] prp = line.Split(':');
			string key = prp[0];
			string val = prp[1];
			Property[key] = val;
		}

		stream.Close ();
	}

	public static void Save()
	{
		string path = Application.persistentDataPath + "/playerprogress.sav";

		if (!File.Exists (path))
			File.Create (path);

		StreamWriter stream = new StreamWriter (path);
		foreach (KeyValuePair<string, string> entry in Property)
		{
			stream.Write(entry.Key + ":" + entry.Value);
			stream.WriteLine ();
		}
		stream.Flush ();
		stream.Close ();
	}

	public static void Print()
	{
		foreach (KeyValuePair<string, string> entry in Property)
		{
			Debug.Log(entry.Key + " = " + entry.Value);
		}
	}
}
