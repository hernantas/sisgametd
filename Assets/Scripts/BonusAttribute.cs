﻿using UnityEngine;
using System.Collections;

public class BonusAttribute
{
	public string name = "";
	public int priority = 0;
	public float damage = 0;
	public float speed = 0;
	public float range = 0;
	public float maxHealth = 0;
}