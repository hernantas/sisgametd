﻿using UnityEngine;
using System.Collections;

public class Missile : MonoBehaviour {
	public GameObject caster { set; get; }
	public float Damage { set; get; }
	public float AreaOfEffect { set; get; }
	public float Speed;
	// Only affect if area of effect > 0
	public bool canAreaGround { set; get; }
	public bool canAreaAir { set; get; }
	public GameObject Target { set; get; }
	public bool isProjectile  { set; get; }
	private Vector3 targetPos;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Speed += (Speed / 10 * Time.deltaTime);

		if (Target != null)
			targetPos = Target.transform.position;
		this.transform.LookAt (targetPos);
		targetPos.y = 0;
		Vector3 missilePos = this.transform.position;
		missilePos.y = 0;

		Vector3 direction = targetPos - this.transform.position;
		this.transform.position += (direction.normalized * Speed * Time.deltaTime);
		
		if (Vector3.Distance(targetPos, this.transform.position) < Speed * Time.deltaTime)
		{
			if (AreaOfEffect == 0)
			{
				if (Target != null)
				{
					Target.GetComponent<Monster>().doDamage(Damage);
				}
			}
			else
			{
				GameObject[] goList = GameObject.FindGameObjectsWithTag("Monster");
				
				foreach(GameObject go in goList)
				{
					Vector3 tPos = go.transform.position;
					tPos.y = targetPos.y;
					
					if (Vector3.Distance(tPos, targetPos) < AreaOfEffect && ((go.GetComponent<Monster>().IsFlying && canAreaAir) || (!go.GetComponent<Monster>().IsFlying && canAreaGround)))
					{
						go.GetComponent<Monster>().doDamage(Damage);
					}
				}
			}

			Destroy(this.gameObject);
		}

		// Set Y
		/*
		Vector3 casterPos = caster.transform.position;
		casterPos.y = 0;

		float distanceCasterToTarget = Vector3.Distance (casterPos, targetPos);
		float distanceToTarget = Vector3.Distance (missilePos, targetPos);

		if (distanceToTarget/distanceCasterToTarget > 0.25f)
			this.transform.position = new Vector3(this.transform.position.x, Mathf.Sin ((distanceToTarget / distanceCasterToTarget)* Mathf.PI) * 10, this.transform.position.z);
			*/
	}
}
