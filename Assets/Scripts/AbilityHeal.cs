﻿using UnityEngine;
using System.Collections;

public class AbilityHeal : Ability {

	public AbilityHeal()
	{
		name = "Heal";
		cooldown = 30;
		duration = 1;
	}

	protected override bool CastCondition ()
	{
		return (!caster.IsDeath && caster.Health / caster.MaxHealth < 0.5f);
	}

	protected override void OnDuration ()
	{
		caster.Health += caster.MaxHealth * 0.5f * Time.deltaTime;
	}
}
