﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

class LevelDetail
{
	public int LevelNumber { set; get; }
	private int _star;
	public int Star 
	{ 
		set
		{
			if (value == -1 || (value >= 0 && value <= 3))
			{
				_star = value;
			}
			else
				_star = -1;
		}
		get
		{
			return _star;
		}
	}
}

public class MainMenu : MonoBehaviour {
	List<string> menu = new List<string> ();
	List<LevelDetail> levels = new List<LevelDetail>();
	int page = 0;
	public Texture2D logo;
	public Texture2D starTx;
	public Texture2D starDisTx;
	public Texture2D btnBg;
	private GUIStyle guiStyle;
	private GUIStyle btnStyle;
	public Texture2D bgExit;
	public Texture2D bgNext;
	public Texture2D bgPrev;
	public Texture2D bgMainMenu;

	void OnGUI()
	{
		GUI.DrawTexture (new Rect (0,0,Screen.width,Screen.height), bgMainMenu);
		GUI.DrawTexture (new Rect(20,20,256,128), logo);

		for (int i=0;i<menu.Count;i++)
		{
			if (GUI.Button (new Rect(Screen.width-400,(Screen.height/2.0f)-((menu.Count/2)*50)+(i*60),300,50), menu[i], btnStyle))
			{
				OnMenuClick(menu[i]);
			}
		}

		int blockPerRow = 10;
		int blockPerCol = 5;

		int start = (page) * blockPerCol * blockPerRow;
		for (int i=start;i<Mathf.Clamp((page+1)*blockPerCol*blockPerRow,0, levels.Count);i++)
		{
			float left = ((i-start)%blockPerRow*65)+(Screen.width/2-(blockPerRow/2)*65);
			float top = 100+(65*Mathf.CeilToInt((i-start)/blockPerRow));
			Rect rect = new Rect(left, top,50,50);
			if (levels[i].LevelNumber == 1 || levels[i-1].Star >= 1)
			{
				if (GUI.Button (rect, levels[i].LevelNumber.ToString(), btnStyle))
				{
					ApplicationLevel.Continous = false;
					ApplicationLevel.Lives = 10;
					ApplicationLevel.LevelToStart = levels[i].LevelNumber;
					ApplicationLevel.QuestStatus = QuestStatus.IDLE;
					Application.LoadLevel(1);
				}
			}
			else
			{
				GUI.Button (rect, "["+levels[i].LevelNumber.ToString()+"]", btnStyle);
			}

			for (int s=0; levels[i].Star != -1 && s < 3; s++)
			{
				if (s < levels[i].Star)
					GUI.DrawTexture(new Rect(left+(s*16f), top+50-7.5f, 15, 15), starTx);
				else
					GUI.DrawTexture(new Rect(left+(s*16f), top+50-7.5f, 15, 15), starDisTx);
        	}
		}

		if (levels.Count > 0)
		{
			if (page > 0 && GUI.Button(new Rect(20,Screen.height-70, 50, 50), bgPrev, guiStyle))
			{
				page --;
			}

			if (page < levels.Count/(blockPerCol*blockPerRow)-1 && GUI.Button(new Rect(Screen.width-140,Screen.height-70, 50,50), bgNext, guiStyle))
			{
				page ++;
			}

			if (GUI.Button(new Rect(Screen.width-70,20, 50,50), bgExit, guiStyle))
			{
				OnMenuClick("Menu");
			}
		}
	}

	void AllClear()
	{
		menu.Clear ();
		levels.Clear ();
	}

	void ShowMainMenu()
	{
		AllClear ();
		menu.Add ("Campaign");
		menu.Add ("Survival");
		menu.Add ("Option");
		if (Application.platform == RuntimePlatform.LinuxPlayer || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.WindowsPlayer)
			menu.Add ("Exit");
	}

	void ShowLevel()
	{
		AllClear ();
		for(int i=0;i<100;i++)
		{
			string key = "level-"+(i+1);
			LevelDetail ld = new LevelDetail();

			if (!SaveFile.Property.ContainsKey(key))
			{
				SaveFile.Property[key] = (-1).ToString();
			}

			ld.LevelNumber = i+1;
			ld.Star = int.Parse(SaveFile.Property[key]);
			if (ld.Star == 0)
				ld.Star = -1;
			levels.Add(ld);
		}
		SaveFile.Save ();
	}

	void OnMenuClick(string menustr)
	{
		switch(menustr)
		{
		case "Menu":
			ShowMainMenu();
			break;
		case "Campaign":
			ShowLevel();
			break;
		case "Survival":
			ApplicationLevel.Continous = true;
			ApplicationLevel.Lives = 10;
			ApplicationLevel.LevelToStart = 1;
			ApplicationLevel.QuestStatus = QuestStatus.IDLE;
			Application.LoadLevel(1);
			break;
		case "Exit":
			Application.Quit();
			break;
		}
	}

	void Start()
	{
		guiStyle = new GUIStyle(GetComponent<UtilityAsset>().guiStyle);
		btnStyle = new GUIStyle (GetComponent<UtilityAsset> ().guiStyle);
		btnStyle.normal.background = btnBg;
		btnStyle.alignment = TextAnchor.MiddleCenter;

		if (ApplicationLevel.QuestStatus == QuestStatus.SUCCESS_1 || 
		    ApplicationLevel.QuestStatus == QuestStatus.SUCCESS_2 || 
		    ApplicationLevel.QuestStatus == QuestStatus.SUCCESS_3 || 
		    ApplicationLevel.QuestStatus == QuestStatus.FAIL)
		{
			int point = 0;
			switch (ApplicationLevel.QuestStatus)
			{
			case QuestStatus.FAIL: point = -1; break;
			case QuestStatus.SUCCESS_1: point = 1; break;
			case QuestStatus.SUCCESS_2: point = 2; break;
			case QuestStatus.SUCCESS_3: point = 3; break;
			}

			if (int.Parse(SaveFile.Property["level-"+ApplicationLevel.LevelToStart]) < point)
				SaveFile.Property["level-"+ApplicationLevel.LevelToStart] = point.ToString();

			SaveFile.Save();
			ShowLevel();
		}
		else
		{
			SaveFile.Load();
			ShowMainMenu ();
		}
	}
}
