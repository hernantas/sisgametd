﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Monster : MonoBehaviour
{
	public AttributeManager attrMgr = new AttributeManager ();
	private int currentWaypoint = 0;
	public List<Vector3> WaypointList;
	public float baseHealth = 10;
	public float health;
	public float Health { 
		set
		{
			health = value;
		}
		get
		{
			return health;
		}
	}
	private float maxHealth;
	public float MaxHealth 
	{ 
		set
		{
			maxHealth = value;
		}
		get
		{
			return maxHealth+attrMgr.Bonus.maxHealth;
		}
	}
	public bool IsDeath 
	{ 
		set
		{
			if (value)
				Health = 0;
		}
		get
		{
			return (Health <= 0);
		}
	}
	public float baseMoveSpeed = 5;
	public float MoveSpeed 
	{ 
		set
		{
			baseMoveSpeed = value;
		}
		get
		{
			return baseMoveSpeed + attrMgr.Bonus.speed;
		}
	}
	public float flyingHeight = 0;
	public bool IsFlying
	{
		get
		{
			return flyingHeight > 0;
		}
	}
	public float decalTime = 1.75f;
	public int GoldBonus { set; get; }
	private Texture2D bgImage, fgImage;
	private GUIStyle goldStyle;
	private float lifetime = 0;
	void Start()
	{
		this.bgImage = Camera.main.GetComponent<UtilityAsset> ().healthBgTex;
		this.fgImage = Camera.main.GetComponent<UtilityAsset> ().healthFgTex;
		goldStyle = new GUIStyle(Camera.main.GetComponent<UtilityAsset> ().guiStyle);
		goldStyle.normal.textColor = new Color (1.0f, 0.843137f, 0.0f);
	}

	void OnGUI()
	{
		Vector3 screenPos = Camera.main.WorldToScreenPoint (this.transform.position);
		screenPos.y = Screen.height - screenPos.y;
		
		if (screenPos.x > 0 && screenPos.y > 0 && screenPos.x < Screen.width && screenPos.y < Screen.height && Health/MaxHealth < 1.0f)
		{
			if (IsDeath)
			{
				GUI.Label(new Rect (screenPos.x-50,screenPos.y-40+(decalTime*5), 50,18), "+" + Mathf.CeilToInt(MaxHealth/50) + " G", goldStyle);
			}
			else
			{
				// Create one Group to contain both images
				// Adjust the first 2 coordinates to place it somewhere else on-screen
				GUI.BeginGroup (new Rect (screenPos.x-50,screenPos.y-20, 75,10));
				
				// Draw the background image
				GUI.DrawTexture (new Rect (0,0, 75,10), bgImage, ScaleMode.StretchToFill);
				
				// Create a second Group which will be clipped
				// We want to clip the image and not scale it, which is why we need the second Group
				GUI.BeginGroup (new Rect (0,0, Health / MaxHealth * 75, 10));
				
				// Draw the foreground image
				GUI.DrawTexture (new Rect (0,0,75,10), fgImage, ScaleMode.StretchToFill);
				
				// End both Groups
				GUI.EndGroup ();
				
				GUI.EndGroup ();
			}
		}
	}

	void Update()
	{
		// Debug.Log (Health);

		if (IsDeath)
		{
			ActionDeath ();
		}
		else
		{
			ActionAlive();

			lifetime += Time.deltaTime;
			//doDamage(Random.Range(lifetime, lifetime*2));
		}
	}

	void ActionDeath()
	{
		decalTime -= Time.deltaTime;
		this.transform.Translate (0, -2 * Time.deltaTime, 0);

		if (decalTime <= 0)
		{
			DestroyObject(this.gameObject);
		}
	}
	void ActionAlive()
	{
		Vector3 pos = transform.position;
		pos.y = WaypointList [currentWaypoint].y;
		float distance = Vector3.Distance(pos, WaypointList[currentWaypoint]);
		float distanceToMove = MoveSpeed * Time.deltaTime;


		if (distance <= distanceToMove)
		{
			this.transform.position = WaypointList[currentWaypoint];
			currentWaypoint++;

			if (currentWaypoint == this.WaypointList.Count)
			{
				currentWaypoint = 0;
				this.transform.position = WaypointList[currentWaypoint];
				Camera.main.GetComponent<Player>().Live -= 1;
			}
		}
		else
		{
			Vector3 direction = WaypointList[currentWaypoint] - pos;
			this.transform.position += (direction.normalized * distanceToMove);
		}

		Ability[] abs = GetComponents<Ability> ();
		foreach(Ability ab in abs)
		{
			ab.cast();
		}

		Vector3 hpos = transform.position;
		hpos.y = GetComponent<MeshFilter> ().mesh.bounds.size.y / 2.0f + flyingHeight;
		transform.position = hpos;

		Vector3 dirFace = WaypointList [currentWaypoint];
		dirFace.y = transform.position.y;
		this.transform.LookAt(dirFace);
	}

	public void doDamage(float damage)
	{
		if (!IsDeath && (Health-damage) <= 0)
		{
			Camera.main.GetComponent<Player>().Gold += Mathf.CeilToInt(MaxHealth/50);
		}

		Health -= damage;
	}
}

/*
public class Monster : MonoBehaviour 
{
	public GameObject[] WaypointList;
	public float MoveSpeed = 5;
	private float curHealth = 50;
	public float maxHealth = 50;
	private int curTarget = 1;
	private bool death = false;
	private float decalTime = 1.75f;
	private Texture2D bgImage; 
	private Texture2D fgImage; 

	// Use this for initialization
	void Start () 
	{
		curHealth = maxHealth;
		UtilityAsset asset = Camera.main.GetComponent<UtilityAsset> ();
		this.bgImage = asset.healthBgTex;
		this.fgImage = asset.healthFgTex;

		this.tag = "Monster";
	}

	void OnGUI()
	{
		Vector3 screenPos = Camera.main.WorldToScreenPoint (this.transform.position);
		screenPos.y = Screen.height - screenPos.y;

		if (screenPos.x > 0 && screenPos.y > 0 && screenPos.x < Screen.width && screenPos.y < Screen.height && curHealth/maxHealth < 1.0f)
		{
			if (death)
			{
				GUI.Label(new Rect (screenPos.x-50,screenPos.y-40+(decalTime*5), 50,18), "+" + Mathf.CeilToInt(maxHealth/50) + "G");
			}
			else
			{
				// Create one Group to contain both images
				// Adjust the first 2 coordinates to place it somewhere else on-screen
				GUI.BeginGroup (new Rect (screenPos.x-50,screenPos.y-20, 75,10));
				
				// Draw the background image
				GUI.DrawTexture (new Rect (0,0, 75,10), bgImage, ScaleMode.StretchToFill);
				
				// Create a second Group which will be clipped
				// We want to clip the image and not scale it, which is why we need the second Group
				GUI.BeginGroup (new Rect (0,0, curHealth / maxHealth * 75, 10));
				
				// Draw the foreground image
				GUI.DrawTexture (new Rect (0,0,75,10), fgImage, ScaleMode.StretchToFill);
				
				// End both Groups
				GUI.EndGroup ();
				
				GUI.EndGroup ();
			}
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (death) 
		{
			decalTime -= Time.deltaTime;
			
			if (decalTime < 0f)
				Destroy(this.gameObject);
		}
		else
		{
			float distanceToMove = MoveSpeed * Time.deltaTime;

			if (Vector3.Distance(WaypointList[this.curTarget].transform.position,
			                     this.transform.position) < distanceToMove)
			{
				this.transform.position = WaypointList[this.curTarget].transform.position;

				curTarget++;
			}

			if (curTarget < WaypointList.Length)
			{
				Vector3 direction = WaypointList [curTarget].transform.position - this.transform.position;
				direction.Normalize ();
				this.transform.Translate (direction * distanceToMove);
			}
			else
			{
				curTarget = 0;
				this.transform.position = WaypointList[curTarget].transform.position;
				Camera.main.GetComponent<Player> ().Live--;
			}
		}

	}

	public void Death()
	{
		death = true;
		this.renderer.enabled = false;
		Camera.main.GetComponent<Player> ().Gold += Mathf.CeilToInt(maxHealth/50);
	}

	public bool isDeath()
	{
		return death;
	}

	public void Damage(float dmg)
	{
		curHealth -= dmg;

		if (curHealth <= 0)
			Death();
	}
}
*/
