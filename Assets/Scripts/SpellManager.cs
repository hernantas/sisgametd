﻿using UnityEngine;
using System.Collections;

public class SpellManager : MonoBehaviour {
	public GameObject AOEObject;
	private int spellIndex = -1;
	private bool delay = false;
	private Spell[] spells;
	private GUIStyle btnGuiSyle;
	private GUIStyle btnLblStyle;
	private Texture2D btnBgTex;

	void OnGUI()
	{
		if (GetComponent<Quest>().status == QuestStatus.ONPROGRESS)
		{
			int index = 0;
			foreach(Spell sp in spells)
			{
				if (GetComponent<Spawner>().Level >= sp.MinimumLevel)
				{
					GUI.DrawTexture(new Rect(10+(index*110),Screen.height-110,100,100), btnBgTex);
					GUI.DrawTexture(new Rect(10+(index*110),Screen.height-110,100,100), sp.icon);
					if (GUI.Button(new Rect(10+(index*110),Screen.height-110,100,100), sp.Name, btnGuiSyle))
					{
						delay = true;
						spellIndex = index;
					}
				}
				index++;
			}
		}
	}

	// Use this for initialization
	void Start () {
		spells = GetComponents<Spell> ();
		AOEObject.renderer.enabled = false;
		btnBgTex = GetComponent<UtilityAsset> ().btnTowerBgTex;
		btnGuiSyle = new GUIStyle (GetComponent<UtilityAsset> ().guiStyle);
		btnGuiSyle.normal.background = GetComponent<UtilityAsset> ().btnTowerTex;
		btnGuiSyle.alignment = TextAnchor.LowerRight;
		btnLblStyle = new GUIStyle (GetComponent<UtilityAsset> ().guiStyle);
		btnLblStyle.alignment = TextAnchor.LowerRight;
		btnLblStyle.normal.textColor = Color.white;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 castPost = Builder.getMousePoint();
		castPost.y = 1;
		if (!delay)
		{
			if (spellIndex != -1 && Input.GetMouseButtonDown(1))
			{
				spellIndex = -1;
			}
			if (spellIndex != -1 && Input.GetMouseButtonDown(0))
			{
				spells[spellIndex].cast(castPost);
				spellIndex = -1;
			}
		}
		if (spellIndex != -1)
		{
			AOEObject.renderer.enabled = true;
			AOEObject.transform.localScale = new Vector3(spells[spellIndex].areaOfEffect, spells[spellIndex].areaOfEffect, 1);
			AOEObject.transform.position = castPost;
		}
		else
		{
			AOEObject.renderer.enabled = false;
		}

		delay = false;
	}

	void LateUpdate()
	{

	}
}
