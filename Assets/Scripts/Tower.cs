﻿using UnityEngine;
using System.Collections;

public class Tower : MonoBehaviour
{
	public AttributeManager attrMgr = new AttributeManager ();
	public int level = 1;
	public int maxLevel = 10;
	public float areaOfEffect = 0;
	public float baseDamage = 0;
	public float BaseDamage
	{
		get
		{
			return (this.baseDamage * Mathf.Pow(this.level, 2));
		}
	}
	public float Damage
	{
		get
		{
			return BaseDamage+attrMgr.Bonus.damage;
		}
	}
	public float DamageNextLevel
	{
		get
		{
			return this.baseDamage * Mathf.Pow(this.level+1,2);
		}
	}
	public bool attackGround = true;
	public bool attackAir = true;
	public float baseAttackSpeed = 1.3f;
	public float AttackSpeed 
	{
		get
		{
			return baseAttackSpeed/(1+attrMgr.Bonus.speed);
		}
	}
	private float curAttackSpeed = 0;
	public GameObject attackMissile;
	public float baseRange = 10;
	public float Range
	{
		get
		{
			return this.baseRange + attrMgr.Bonus.range;
		}
	}
	public int basePrice = 0;
	public int Price
	{
		get
		{
			return this.basePrice * this.level;
		}
	}
	public int PriceNextLevel
	{
		get
		{
			return this.basePrice * (this.level);
		}
	}
	public bool InBuildMode { set; get; }
	private GameObject curAttackTarget = null;
	public bool isProjectile = false;

	public void LevelUp()
	{
		if (this.level < maxLevel || this.maxLevel == 0)
			this.level++;
	}

	Tower()
	{
		attrMgr = new AttributeManager ();
	}

	void Start()
	{
	}

	void Update()
	{
		if (!this.InBuildMode)
		{
			FindObjectToAttack();
			if(curAttackTarget != null)
			{
				Vector3 direction = curAttackTarget.transform.position;
				direction.y = this.transform.position.y;
				this.transform.LookAt(direction);
			}
		}
	}

	public bool IsAttackReady()
	{
		return (curAttackSpeed <= 0);
	}

	public void FindObjectToAttack()
	{
		if (!IsAttackReady())
			curAttackSpeed -= Time.deltaTime;
		else
		{
			if (curAttackTarget == null)
			{
				GameObject[] targets = GameObject.FindGameObjectsWithTag("Monster");
				float closest = Range;

				foreach (GameObject target in targets)
				{
					float distance = Vector3.Distance(target.transform.position, this.transform.position); 
					Monster mos = target.GetComponent<Monster>();
					if (distance < closest && !mos.IsDeath && ((this.attackGround && !mos.IsFlying) || (this.attackAir && mos.IsFlying)))
					{
						curAttackTarget = target;
						closest = distance;
					}
				}
			}
			else
			{
				if (curAttackTarget.GetComponent<Monster>().IsDeath || 
				    Vector3.Distance(curAttackTarget.transform.position, this.transform.position) > Range)
					curAttackTarget = null;
				else
				{
					curAttackSpeed = AttackSpeed;
					Vector3 spawnPos = this.transform.position;
					spawnPos.y = renderer.bounds.size.y*3/4;
					GameObject missile = (GameObject)Instantiate(attackMissile, spawnPos, Quaternion.identity);
					Missile msl = missile.GetComponent<Missile>();
					msl.Damage = Damage;
					msl.AreaOfEffect = areaOfEffect;
					msl.Target = curAttackTarget;
					msl.caster = this.gameObject;
					msl.canAreaAir = attackAir;
					msl.canAreaGround = attackGround;
					msl.isProjectile = isProjectile;
				}
			}
		}
	}
}

/*
public class Tower : MonoBehaviour {
	public int Level = 1;
	private float BaseDamage = 10;
	public float Damage = 10;
	private float curAspd = 0;
	public float AttackSpeed = 1.5f;
	public GameObject TowerMissile;
	public float Range = 10;
	public int Price = 10;
	private int basePrice = 0;
	private bool towerActive = false;
	private GameObject curTarget = null;

	// Use this for initialization
	void Start () {
		BaseDamage = Damage;
		basePrice = Price;

		Upgrade (false);
	}

	int getPrice(int nextLevel)
	{
		return (int) Mathf.Ceil(basePrice * nextLevel * Mathf.Sqrt (nextLevel));
	}

	public int getNextPrice()
	{
		return getPrice (Level + 1);
	}

	public float getNextDamage()
	{
		return (Level+1) * BaseDamage;
	}

	public void Upgrade()
	{
		Upgrade (true);
	}

	public void Upgrade(bool b)
	{
		if (b) Level++;
		Damage = Level * BaseDamage;
		Price = getPrice(Level);
	}
	
	// Update is called once per frame
	void Update () {
		if (towerActive)
		{

			if (curTarget == null)
			{
				GameObject[] targets = GameObject.FindGameObjectsWithTag("Monster");

				float closest = Range;

				for (int i=0;i<targets.Length;i++)
				{
					float dist = Vector3.Distance(targets[i].transform.position, this.transform.position);

					if (!targets[i].GetComponent<Monster>().isDeath() && dist < closest)
					{
						curTarget = targets[i];
						closest = dist;
					}
				}
			}
			else
			{
				if (curTarget.GetComponent<Monster>().isDeath())
					curTarget = null;

				else if (curAspd <= 0)
				{
					if (Vector3.Distance(curTarget.transform.position, transform.position) < Range)
					{
						Vector3 spawnPos = transform.position;
						spawnPos.y += renderer.bounds.size.y*3/4;
						GameObject go = (GameObject) Instantiate(TowerMissile, spawnPos, Quaternion.identity);
						Missile ms = go.GetComponent<Missile>();
						ms.Damage = Damage;
						ms.Target = curTarget;
						curAspd = AttackSpeed;
					}
					else
					{
						curTarget = null;
					}
				}
			}
		}

		if (curAspd > 0)
			curAspd -= Time.deltaTime;
	}

	public void setActive(bool b)
	{
		towerActive = b;
	}
	public bool isActive()
	{
		return towerActive;
	}
}
*/