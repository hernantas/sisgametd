﻿using UnityEngine;
using System.Collections;

public class SpellThunder : Spell {
	SpellThunder()
	{
		Name = "Burn";
		areaOfEffect = 5;
		duration = 10;
		
		affectedTag = "Monster";
	}
	
	public override void OnTick(GameObject go, SpellCasted sc)
	{
		Monster mos = go.GetComponent<Monster> ();
		float damage = mos.Health * 0.2f * Time.deltaTime;
		mos.doDamage (damage);
		sc.resetTick = true;
	}
}
