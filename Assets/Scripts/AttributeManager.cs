﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AttributeManager
{
	private List<BonusAttribute> bonus = new List<BonusAttribute>();
	private BonusAttribute extraAttribute = new BonusAttribute();
	public BonusAttribute Bonus
	{
		get
		{
			return extraAttribute;
		}
	}
	public int Count
	{
		get
		{
			return bonus.Count;
		}
	}
	
	public void Add(BonusAttribute attr)
	{
		bool add = true;
		for(int i=0;i<bonus.Count;i++)
		{
			if (bonus[i].name == attr.name)
			{
				if (bonus[i].priority < attr.priority)
				{
					bonus[i] = attr;
				}
				add = false;
				break;
			}
		}

		if (add)
			bonus.Add(attr);

		Calculate();
	}
	public void Remove(string name)
	{
		for (int i=0;i<bonus.Count;i++)
		{
			if (bonus[i].name == name)
			{
				bonus.Remove(bonus[i]);
				break;
			}
		}

		Calculate();
	}
	private void Calculate()
	{
		extraAttribute = new BonusAttribute();
		extraAttribute.name = "Extra";
		extraAttribute.priority = 0;
		extraAttribute.damage = 0;
		extraAttribute.speed = 0;
		extraAttribute.range = 0;
		
		foreach(BonusAttribute att in bonus)
		{
			extraAttribute.damage += att.damage;
			extraAttribute.speed += att.speed;
			extraAttribute.range += att.range;
		}
	}
}