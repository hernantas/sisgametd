﻿using UnityEngine;
using System.Collections;

public class Quest : MonoBehaviour 
{
	public QuestStatus status
	{
		set
		{
			ApplicationLevel.QuestStatus = value;
		}
		get
		{
			return ApplicationLevel.QuestStatus;
		}
	}
	protected string message = "Live: 10\n" +
		"Starting Gold: 10\n\n" +
		"Try to survive all wave with all the live";
	private GUIStyle guiStyle;
	private GUIStyle btnStyle;
	private Texture2D bgPanel;

	void Start()
	{
		guiStyle = new GUIStyle(GetComponent<UtilityAsset> ().guiStyle);

		btnStyle = new GUIStyle(GetComponent<UtilityAsset> ().guiStyle);
		btnStyle.normal.background = GetComponent<UtilityAsset> ().btnTex;
		btnStyle.alignment = TextAnchor.MiddleCenter;

		bgPanel = GetComponent<UtilityAsset> ().bgPanel;
	}

	void OnGUI()
	{
		if (status == QuestStatus.IDLE)
		{
			float width = 500;
			float height = 300;
			GUI.DrawTexture(new Rect(Screen.width/2-width/2, Screen.height/2-height/2-65, width, height+65), bgPanel); 
			GUI.Label (new Rect (Screen.width/2-100/2, Screen.height/2-height/2-45, 100, 50), "Message", guiStyle);
			GUI.Label (new Rect (Screen.width/2-width/2+30, Screen.height/2-height/2, width-30, height), message, guiStyle);

			if (GUI.Button(new Rect (Screen.width/2, Screen.height/2+height/2+10, 100, 50), "Accept", btnStyle))
			{
				status = QuestStatus.ONPROGRESS;
			}
		}
		else if (status == QuestStatus.FAIL || status == QuestStatus.SUCCESS_1 || status == QuestStatus.SUCCESS_2 || status == QuestStatus.SUCCESS_3)
		{
			float width = 500;
			float height = 300;
			string success = "Completed";
			if (status == QuestStatus.FAIL)
				success = "Fail";

			GUI.DrawTexture(new Rect(Screen.width/2-width/2, Screen.height/2-height/2-65, width, height+65), bgPanel); 
			GUI.Label (new Rect (Screen.width/2-100/2, Screen.height/2-height/2-45, 100, 50), success, guiStyle);
			GUI.Label (new Rect (Screen.width/2-width/2+30, Screen.height/2-height/2, width-30, height), message, guiStyle);

			if (status == QuestStatus.FAIL && GUI.Button(new Rect (Screen.width/2-90, Screen.height/2+height/2+10, 100, 50), "Retry", btnStyle))
			{
				Application.LoadLevel(1);
			}
			if (GUI.Button(new Rect (Screen.width/2+90, Screen.height/2+height/2+10, 100, 50), "Return", btnStyle))
			{
				Application.LoadLevel(0);
			}
		}
	}
}
