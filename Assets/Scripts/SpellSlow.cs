﻿using UnityEngine;
using System.Collections;

public class SpellSlow : Spell {
	SpellSlow()
	{
		Name = "Time Slow";
		areaOfEffect = 5;
		duration = 10;

		affectedTag = "Monster";
	}

	public override void OnObjectAdded(GameObject go)
	{
		Monster mos = go.GetComponent<Monster> ();
		BonusAttribute ba = new BonusAttribute ();
		ba.name = "TimeSlow";
		ba.speed = mos.baseMoveSpeed * (Random.Range (6, 8) / 10.0f) * -1;
		mos.attrMgr.Add (ba);
	}
	public override void OnObjectRemoved(GameObject go)
	{
		Monster mos = go.GetComponent<Monster> ();
		mos.attrMgr.Remove ("TimeSlow");
	}
}
